/// See [Window] for the actual documentation.
@JS()
library vsdart.window;

import 'package:meta/meta.dart';
import 'package:js/js.dart';
import 'package:vsdart/utils.dart';

import 'classes.dart';

/// Namespace for dealing with the current window of the editor. That is visible
/// and active editors, as well as, UI elements to show messages, selections,
/// and asking for user input.
///
/// It wraps [VsWindow] for a more Dart-idiomatic API.
class Window {
  late final VsWindow _window;

  /// Show an information message to users. Optionally provide an array of items
  /// which will be presented as clickable buttons.
  void showInformationMessage(String message) =>
      _window.showInformationMessage(message);

  /// Opens an input box to ask the user for input.
  ///
  /// The returned value will be undefined if the input box was canceled
  /// (e.g. pressing ESC). Otherwise the returned value will be the string typed
  /// by the user or an empty string if the user did not type anything but
  /// dismissed the input box with OK
  Future<String?> showInputBox([InputBoxOptions? options]) =>
      thenableToFuture<String?>(_window.showInputBox(options));

  /// Shows a simple selection list that only allows a single selection.
  ///
  /// For more complex control over the list, use [showQuickPick] or
  /// [showMultiQuickPick]; to allow multiple selections, use
  /// [showSimpleMultiQuickPick].
  Future<String?> showSimpleQuickPick(List<String> items) =>
      thenableToFuture<String?>(_window.showQuickPick<String>(items));

  /// Shows a simple selection list that allows multiple selections.
  ///
  /// For more complex control over the list, use [showQuickPick] or
  /// [showMultiQuickPick]; to only allow a single selection, use
  /// [showSimpleQuickPick].
  Future<List<String>?> showSimpleMultiQuickPick(List<String> items) async {
    final results = await thenableToFuture<List?>(
        _window.showQuickPick(items, VsQuickPickOptions(canPickMany: true)));
    if (results == null) {
      return null;
    } else {
      return List<String>.from(results);
    }
  }

  /// Show a selection list that allows a single selection.
  ///
  /// Use [options] to configure the behavior of the selection list. [T] must be
  /// a [QuickPickItem] or a class that extends it.
  ///
  /// For a simpler list, use [showSimpleQuickPick] or
  /// [showSimpleMultiQuickPick]; to allow multiple selections, use
  /// [showMultiQuickPick].
  Future<T?> showQuickPick<T extends QuickPickItem>(List<T> items,
          [QuickPickOptions? options]) =>
      thenableToFuture<T?>(
          _window.showQuickPick<T>(items, options?.toVs(overrideWith: false)));

  /// Show a selection list that only allows multiple selections.
  ///
  /// Use [options] to configure the behavior of the selection list. [T] must be
  /// a [QuickPickItem] or a class that extends it.
  ///
  /// For a simpler list, use [showSimpleQuickPick] or
  /// [showSimpleMultiQuickPick]; to only allow a single selection, use
  /// [showQuickPick].
  Future<List<T>?> showMultiQuickPick<T extends QuickPickItem>(List<T> items,
      [QuickPickOptions? options]) async {
    final results = await thenableToFuture<List?>(
        _window.showQuickPick(items, options?.toVs(overrideWith: true)));
    if (results == null) {
      return null;
    } else {
      return List<T>.from(results);
    }
  }

  Window(this._window);
}

/// Namespace for dealing with the current window of the editor. That is visible
/// and active editors, as well as, UI elements to show messages, selections,
/// and asking for user input.
///
/// Use the wrapper [Window] for a more Dart-idiomatic API.
@JS('Window')
@anonymous
abstract class VsWindow {
  external void showInformationMessage(String message);
  external Thenable showInputBox([InputBoxOptions? options]);
  external Thenable showQuickPick<T>(List<T> items,
      [VsQuickPickOptions? options]);
}

/// Options to configure the behavior of the input box UI.
@JS()
@anonymous
class InputBoxOptions {
  /// Set to `true` to keep the input box open when focus moves to another part
  /// of the editor or to another window.
  ///
  /// This setting is ignored on iPad and is always false.
  external bool? ignoreFocusOut;

  /// Controls if a password input is shown. Password input hides the typed
  /// text.
  external bool? password;

  /// An optional string to show as placeholder in the input box to guide the
  /// user what to type.
  external String? placeHolder;

  /// The text to display underneath the input box.
  external String? prompt;

  /// An optional string that represents the title of the input box.
  external String? title;

  /// The value to prefill in the input box.
  external String? value;

  /// Selection of the prefilled {@@linkcode InputBoxOptions.value value}.
  /// Defined as tuple of two number where the first is the inclusive start
  /// index and the second the exclusive end index. When undefined the whole
  /// word will be selected, when empty (start equals end) only the cursor will
  /// be set, otherwise the defined range will be selected
  external dynamic _valueSelection;

  /// An optional function that will be called to validate input and to give a
  /// hint to the user.
  // TODO: this should be FutureOr<String>
  external String? Function(String value) validateInput();

  external factory InputBoxOptions({
    bool? ignoreFocusOut,
    bool? password,
    String? placeHolder,
    String? prompt,
    String? title,
  });
}

/// Represents an item that can be selected from a list of items.
@JS()
@anonymous
class QuickPickItem {
  /// Always show this item.
  external bool? alwaysShow;

  /// A human-readable string which is rendered less prominent in the same line.
  ///
  /// Supports rendering of theme icons via the `$(<name>)`-syntax.
  external String? description;

  /// A human-readable string which is rendered less prominent in a separate
  /// line. Supports rendering of theme icons via the `$(<name>)`-syntax.
  external String? detail;

  /// A human-readable string which is rendered prominent.
  ///
  /// Supports rendering of theme icons via the `$(<name>)`-syntax.
  external String label;

  /// Optional flag indicating if this item is picked initially. (Only honored when the picker allows multiple selections.)
  external bool? picked;

  external factory QuickPickItem({
    @required String label,
    bool? alwaysShow,
    String? description,
    String? detail,
    bool? picked,
  });
}

/// Options to configure the behavior of the quick pick UI.
///
/// It wraps [VsQuickPickOptions] for a more Dart-idiomatic API.
class QuickPickOptions {
  /// An optional flag to make the picker accept multiple selections, if true
  /// the result is an array of picks.
  bool? canPickMany;

  /// Set to `true` to keep the picker open when focus moves to another part of
  /// the editor or to another window. This setting is ignored on iPad and is
  /// always `false`.
  bool? ignoreFocusOut;

  /// An optional flag to include the description when filtering the picks.
  bool? matchOnDescription;

  /// An optional flag to include the detail when filtering the picks
  bool? matchOnDetail;

  /// An optional string to show as placeholder in the input box to guide the
  /// user what to pick on.
  String? placeHolder;

  /// An optional string that represents the title of the quick pick.
  String? title;

  /// An optional function that is invoked whenever an [item] is selected.
  ///
  /// [item] can be only [String] or [QuickPickItem]
  dynamic Function(dynamic item)? onDidSelectItem;

  QuickPickOptions({
    this.canPickMany,
    this.ignoreFocusOut,
    this.matchOnDescription,
    this.matchOnDetail,
    this.placeHolder,
    this.title,
    this.onDidSelectItem,
  });

	/// Convert [QuickPickOptions] to [VsQuickPickOptions] to use with native JS
	/// functions.
	/// 
	/// [overrideWith] determines whether to override the [canPickMany] property
	/// with the provided value.
  VsQuickPickOptions toVs({bool? overrideWith}) {
    return onDidSelectItem == null
        ? VsQuickPickOptions(
            canPickMany: overrideWith ?? canPickMany,
            ignoreFocusOut: ignoreFocusOut,
            matchOnDescription: matchOnDescription,
            matchOnDetail: matchOnDetail,
            placeHolder: placeHolder,
            title: title,
          )
        : VsQuickPickOptions(
            canPickMany: overrideWith ?? canPickMany,
            ignoreFocusOut: ignoreFocusOut,
            matchOnDescription: matchOnDescription,
            matchOnDetail: matchOnDetail,
            placeHolder: placeHolder,
            title: title,
            onDidSelectItem: allowInterop(onDidSelectItem!),
          );
  }
}

/// Represents an item that can be selected from a list of items.
///
/// Use the wrapper [QuickPickOptions] for a more Dart-idiomatic API.
@JS('QuickPickOptions')
@anonymous
class VsQuickPickOptions {
  /// An optional flag to make the picker accept multiple selections, if true
  /// the result is an array of picks.
  external bool? canPickMany;

  /// Set to `true` to keep the picker open when focus moves to another part of
  /// the editor or to another window. This setting is ignored on iPad and is
  /// always `false`.
  external bool? ignoreFocusOut;

  /// An optional flag to include the description when filtering the picks.
  external bool? matchOnDescription;

  /// An optional flag to include the detail when filtering the picks
  external bool? matchOnDetail;

  /// An optional string to show as placeholder in the input box to guide the
  /// user what to pick on.
  external String? placeHolder;

  /// An optional string that represents the title of the quick pick.
  external String? title;

  /// An optional function that is invoked whenever an [item] is selected.
  ///
  /// [item] can be only [String] or [QuickPickItem]
  external Function? onDidSelectItem;

  external factory VsQuickPickOptions({
    bool? canPickMany,
    bool? ignoreFocusOut,
    bool? matchOnDescription,
    bool? matchOnDetail,
    String? placeHolder,
    String? title,
    Function? onDidSelectItem,
  });
}
