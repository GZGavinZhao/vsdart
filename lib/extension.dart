@JS()
library _vsdart;

import 'package:js/js.dart';

import 'commands.dart';
import 'window.dart';

/// Loads module with specified [id].
///
/// Note that this is not actually a global variable and when compiled to
/// JavaScript proxies all calls to the `require` function of main application
/// module.
external dynamic require(String id);

VsCode get vscode =>  _vscode ?? require('vscode');
VsCode? _vscode;

var commands = Commands(vscode.commands);
var window = Window(vscode.window);
String get version => vscode.version;

@JS()
@anonymous
abstract class VsCode {
  external VsWindow get window;
  external String get version;
  external VsCommands get commands;
}