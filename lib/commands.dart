/// See [Commands] for the actual documentation.
@JS()
library vsdart.commands;

import 'package:js/js.dart';

import 'classes.dart';

/// Namespace for dealing with commands. In short, a command is a function with
/// a unique identifier. The function is sometimes also called command handler.
///
/// Commands can be added to the editor using the [registerCommand] and
/// [registerTextEditorCommand] functions. Commands can be executed manually or
/// from a UI gesture. Those are:
///
/// - palette - Use the `commands`-section in `package.json` to make a command
/// show in the [command palette](https://code.visualstudio.com/docs/getstarted/userinterface#_command-palette).
/// - keybinding - Use the `keybindings`-section in `package.json` to enable
/// [keybindings](https://code.visualstudio.com/docs/getstarted/keybindings#_customizing-shortcuts)
/// for your extension.
///
/// Commands from other extensions and from the editor itself are accessible to
/// an extension. However, when invoking an editor command not all argument
/// types are supported.
///
/// This is a sample that registers a command handler and adds an entry for that
/// command to the palette. First register a command handler with the identifier
/// `extension.sayHello`.
///
/// ```dart
/// commands.registerCommand('extension.sayHello', () =>
/// 	window.showInformationMessage('Hello World!')
/// );
/// ```
///
/// Second, bind the command identifier to a title under which it will show in
/// the palette (`package.json`).
///
/// ```json
/// "contributes": {
/// 	"commands": [
/// 		{
/// 			"command": "extension.sayHello",
/// 			"title": "Hello World"
/// 		}
/// 	]
/// }
/// ```
class Commands {
  late final VsCommands _commands;

  /// Registers a command that can be invoked via a keyboard shortcut, a menu
  /// item, an action, or directly.
  ///
  /// Registering a command with an existing command identifier twice will cause an error
  ///
  /// [command] is a unique identifier for the command, and [callback] is the
  /// function that is triggered when the command is activated. [thisArg] is
  /// the `this` context used when invoking the handler function, which is
  /// probably never used but provided neverthless.
  Disposable registerCommand(String command, Function callback,
          [dynamic thisArg]) =>
      _commands.registerCommand(command, allowInterop(callback), thisArg);

  Commands(this._commands);
}

@JS('Commands')
@anonymous
abstract class VsCommands {
  external Disposable registerCommand(String command, Function callback,
      [dynamic thisArg]);
}
