/// Support for doing something awesome.
///
/// More dartdocs go here.
@JS()
library vsdart;

import 'package:js/js.dart';

export 'extension.dart';
export 'classes.dart';
export 'utils.dart';
export 'window.dart';

@JS()
class _Exports {
	external set activate(Function function);
	external set deactivate(Function function);
}

@JS()
external _Exports get exports;

void runExtension(Function activate, [Function? deactivate]) {
	exports.activate = allowInterop(activate);
	exports.deactivate = allowInterop(deactivate ?? () {});
}