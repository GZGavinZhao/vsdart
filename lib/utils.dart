/// Utility classes and functions, mostly for dealing with JS interop.
@JS()
library vsdart.utils;

import 'dart:async';

import 'package:js/js.dart';

import 'classes.dart';

/// Creates Dart `Future` which completes when [Thenable] is resolved or
/// rejected.
///
/// See also:
///   - [futureToThenable]
Future<T> thenableToFuture<T>(Thenable thenable) {
  var completer = Completer<T>.sync();
  thenable.then(allowInterop((value) {
    completer.complete(value);
  }), allowInterop((error) {
    completer.completeError(error);
  }));
  return completer.future;
}

/// Creates JS `Thenable` which is resolved when [future] completes.
///
/// See also:
/// - [ThenableToFuture]
Thenable futureToThenable<T>(Future<T> future) {
  return Thenable(allowInterop((Function resolve, Function reject) {
    future.then((result) => resolve(result), onError: reject);
  }));
}
