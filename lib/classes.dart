/// Classes and functions that belong to none of the 13 categories of the
/// Visual Studio Code API.
@JS()
library vsdart.classes;

import 'package:meta/meta.dart';
import 'package:js/js.dart';

@JS()
@anonymous
abstract class Command {
  external String get title;
  external String get command;
  external String? get tooltip;
  external List<dynamic>? get arguments;

  external factory Command({
    @required String title,
    @required String command,
    String tooltip,
  });
}


@JS()
@anonymous
abstract class ExtensionContext {
	external List<dynamic> get subscriptions;
	external String get extensionPath;
}

@JS()
@anonymous
abstract class Disposable{
  // TODO: Implement `static from(...disposableLikes: { dispose: () => any }[]): Disposable;`
	// Probably isn't possible right now because Dart doesn't have functions with variable number of arguments.
  // https://github.com/microsoft/vscode/blob/a80f904d4e9afa7a05ae57a0123c11b727097129/src/vs/vscode.d.ts#L1457

	external dynamic dispose();

  external factory Disposable({Function callOnDispose});
}

@JS()
abstract class Thenable {
  external factory Thenable(
      Function(dynamic Function(dynamic value) resolve,
              dynamic Function(dynamic error) reject)
          executor);
  external Thenable then(dynamic Function(dynamic value) onfulfilled,
      [dynamic Function(dynamic error) onrejected]);
}
