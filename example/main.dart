import 'package:vsdart/vsdart.dart';

const name = 'example.helloWorld';

void main(List<String> args) {
  runExtension(activate);
}

void activate(ExtensionContext context) {
  print('Running...');
  sayHello();
  print(context.extensionPath);

  final disposable = commands.registerCommand(name, () async {
    print('Subscription added!');

    final choice = await window.showSimpleMultiQuickPick(
      ['hi', 'hello'],
    );
    if (choice == null) {
      print('QuickPick cancelled by user.');
    } else {
      // print(choice.label);
      for (var item in choice) {
        print(item);
      }
    }
  });

  context.subscriptions.add(disposable);
}

void sayHello() {
  const msg = 'Hello world!';
  print('Currently in Visual Studio Code version ${vscode.version}');
  print('Trying to call notification...');
  try {
    vscode.window.showInformationMessage(msg);
  } catch (e) {
    print(e);
  }
  print('Success!');
}
