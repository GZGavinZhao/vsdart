import 'dart:io';

import 'package:grinder/grinder.dart';
import 'package:node_preamble/preamble.dart';

main(args) => grind(args);

@DefaultTask()
build() {
  final args = context.invocation.arguments;
  final output = File('main.js');
  Dart2js.compile(
    File('main.dart'),
    outFile: output,
		minify: args.getFlag('release'),
    extraArgs: [
      if (args.getFlag('release')) '-O4',
    ],
  );
  final content = output.readAsStringSync();
  output.writeAsStringSync(getPreamble());
  output.writeAsStringSync(content, mode: FileMode.append);
}
